﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using log4net;

namespace BibiItemExtract
{
    internal class dbAccess
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private System.Data.SqlClient.SqlConnectionStringBuilder _csConnectShip;

        public dbAccess()
        {
            // read and set the default connection string 
            _csConnectShip = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbConnectship);
        }

        public List<String> getActiveAccounts()
        {
            // this is the main method called by our program.  It returns the data to be extracted.

            // this object will eventually hold the records found in the database.  It's structure is set up in dataContracts.cs.
            List<String> response = new List<String>();

            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    // runs the query to find active accounts.  This report doesn't look at Supple, because it of its lengthy transaction history.
                    cmd.CommandText = "SELECT INSTANCE FROM MISC.dbo.EcometryAccounts";

                    cn.ConnectionString = _csConnectShip.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlDataReader rd = cmd.ExecuteReader();

                    if (rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            response.Add(rd[0].ToString());
                        }
                    }

                }
            }
            // return the response object back the method caller
            return response;
        }

        public List<ItemExtract> getItemExtracts (List<String> accounts, DateTime startLookup, DateTime endLookup)
        {
            List<ItemExtract> response = new List<ItemExtract>();

            using (SqlConnection cn = new SqlConnection())
            {
                cn.ConnectionString = _csConnectShip.ToString();
                cn.Open(); 
                
                foreach (string account in accounts)
                {
                    using (SqlCommand cmd = new SqlCommand())

                    {
                        try
                        {
                            // Item Details
                            cmd.CommandText = "SELECT ITM.EDPNO" +
                            ", ITM.ITEMNO" +
                            ", ITM.DESCRIPTION" +
                            ", ALTEDPNO" +
                            ", ACTUALEDPNO" +
                            ", WAREHOUSELOCS_001" +
                            ", substring(ITM.CATEGORY,1,1) as CATEGORY1" +
                            ", substring(ITM.CATEGORY,2,1) as CATEGORY2" +
                            ", substring(ITM.CATEGORY,3,1) as CATEGORY3" +
                            ", substring(ITM.CATEGORY,4,1) as CATEGORY4" +
                            ", CASE WHEN ITM.STOPSHOPDATE <= CONVERT(VARCHAR(10),GETDATE(),112) AND ITM.STOPSHOPDATE > '00000000' THEN 0 ELSE 1 END as AVAILABLE" +
                            ", CASE ITM.STOPSHOPDATE WHEN '00000000' THEN CONVERT(DATETIME,'19000101',120) ELSE CONVERT(DATETIME, ITM.STOPSHOPDATE,120) END as STOPSHOPDATE" +
                            ", CONVERT(numeric(18,2), ITM.PRICE/100) as PRICE" +
                            ", CONVERT(numeric(6,2), left(ITM.POSTHAND,4) + '.' +  right(ITM.POSTHAND,2)) as PH" +
                            ", CONVERT(numeric(6,2), 0) as EXTRAPOSTAGE" +
                            ", CONVERT(numeric(18,2), 0) as LISTPRICE" +
                            ", CONVERT(numeric(18,2), 0) as PRODUCTCOST" +
                            ", ISNULL((SELECT SUM(AVAILABLEINV) FROM " + account + "..INVDETAILS WHERE substring(STATUS,2,1) = 'U' AND EDPNO = ITM.EDPNO),0) as QTYSTOCK" +
                            ", ISNULL((SELECT SUM(AVAILABLEINV) FROM " + account + "..INVDETAILS WHERE substring(STATUS,2,1) = 'U' AND EDPNO = ITM.EDPNO),0) - (ITM.AVAILABLEINV + ITM.CURRENTBO) as ALLOCATED" +
                            ", ITM.STATUS as ITEMSTATUS" +
                            ", ITM.REORDERPOINT" +
                            ", CONVERT(numeric(18,0), ITM.MINORDERQTY) as MINORDER" +
                            ", CONVERT(numeric(8,4), ITM.WEIGHT/10000) as WEIGHT" +
                            ", CONVERT(numeric(6,2), ITM.DEPTH/100) as HEIGHT" +
                            ", CONVERT(numeric(6,2), ITM.WIDTH/100) as WIDTH" +
                            ", CONVERT(numeric(6,2), ITM.LENGTHX/100) as LENGTHX" +
                            ", CASE WHEN RTRIM(SUBSTRING(ITM.FLAGS,9,2)) = '' THEN 0 ELSE 1 END as TAXEXEMPT" +
                            ", 0 as SHIPSEPARATE" +
                            ", ISNULL(RTRIM(SUBSTRING(XRF.REFITEMNO,6,20)),'000000000000') as UPC" +
                            ", '' as TPSKU" +
                            ", CONVERT(numeric(18,2), 0) as HARMONIZATIONCODE" +
                            ", CONVERT(DATETIME, ITM.DATEX ,120) AS DATEX" +
                            ", ITM.USERID " +
                            "FROM " + account + "..ITEMMAST ITM " +
                            "LEFT OUTER JOIN " + account + "..EDPITEMXREF XRF " +
                            "ON ITM.EDPNO = XRF.EDPNO " +
                            "AND XRF.STATUS = 'U' " +
                            "WHERE ITM.DATEX BETWEEN '" + startLookup.ToString("yyyyMMdd") + "' and '" + endLookup.ToString("yyyyMMdd") + "' " +
                            "AND ITM.USERID <> 'BIBIADM'";
                            cmd.Connection = cn;
                            cmd.CommandTimeout = 120;
                            SqlDataReader rdDetails = cmd.ExecuteReader();

                            if (rdDetails.HasRows)
                            {
                                while (rdDetails.Read())
                                {
                                    //parse the rows found and map to the itemExtract object.
                                    ItemExtract ie = new ItemExtract();
                                    ie.clientCode = account.TrimEnd();
                                    ie.thirdPartyID = Convert.ToInt32(rdDetails[rdDetails.GetOrdinal("EDPNO")]);
                                    ie.productNumber = rdDetails[rdDetails.GetOrdinal("ITEMNO")].ToString();
                                    ie.shortDescription = rdDetails[rdDetails.GetOrdinal("DESCRIPTION")].ToString();
                                    ie.longDescription = rdDetails[rdDetails.GetOrdinal("DESCRIPTION")].ToString();
                                    ie.primaryLocation = rdDetails[rdDetails.GetOrdinal("WAREHOUSELOCS_001")].ToString();
                                    ie.productAlias = rdDetails[rdDetails.GetOrdinal("ALTEDPNO")].ToString();
                                    ie.subWithProduct = rdDetails[rdDetails.GetOrdinal("ACTUALEDPNO")].ToString();
                                    ie.category1 = rdDetails[rdDetails.GetOrdinal("CATEGORY1")].ToString();
                                    ie.category2 = rdDetails[rdDetails.GetOrdinal("CATEGORY2")].ToString();
                                    ie.category3 = rdDetails[rdDetails.GetOrdinal("CATEGORY3")].ToString();
                                    ie.category4 = rdDetails[rdDetails.GetOrdinal("CATEGORY4")].ToString();
                                    ie.available = Convert.ToInt32(rdDetails[rdDetails.GetOrdinal("AVAILABLE")]);
                                    ie.notAvailableDate = Convert.ToDateTime(rdDetails[rdDetails.GetOrdinal("STOPSHOPDATE")]);
                                    ie.price = Convert.ToDouble(rdDetails[rdDetails.GetOrdinal("PRICE")]);
                                    ie.postageHandling = Convert.ToDouble(rdDetails[rdDetails.GetOrdinal("PH")]);
                                    ie.extraPostage = Convert.ToDouble(rdDetails[rdDetails.GetOrdinal("EXTRAPOSTAGE")]);
                                    ie.listPrice = Convert.ToDouble(rdDetails[rdDetails.GetOrdinal("LISTPRICE")]);
                                    ie.productCost = Convert.ToDouble(rdDetails[rdDetails.GetOrdinal("PRODUCTCOST")]);
                                    ie.quantityInStock = Convert.ToInt32(rdDetails[rdDetails.GetOrdinal("QTYSTOCK")]);
                                    ie.allocatedQuantity = Convert.ToInt32(rdDetails[rdDetails.GetOrdinal("ALLOCATED")]);
                                    ie.itemStatus = rdDetails[rdDetails.GetOrdinal("ITEMSTATUS")].ToString();
                                    ie.lowStockLimit = Convert.ToInt32(rdDetails[rdDetails.GetOrdinal("REORDERPOINT")]);
                                    ie.reorderAmount = Convert.ToInt32(rdDetails[rdDetails.GetOrdinal("REORDERPOINT")]);
                                    ie.minOrderAmount = Convert.ToInt32(rdDetails[rdDetails.GetOrdinal("MINORDER")]);
                                    ie.weight = Convert.ToInt32(rdDetails[rdDetails.GetOrdinal("WEIGHT")]);
                                    ie.height = Convert.ToInt32(rdDetails[rdDetails.GetOrdinal("HEIGHT")]);
                                    ie.width = Convert.ToInt32(rdDetails[rdDetails.GetOrdinal("LENGTHX")]);
                                    ie.taxExempt = Convert.ToInt32(rdDetails[rdDetails.GetOrdinal("TAXEXEMPT")]);
                                    ie.shipSeparate = Convert.ToInt32(rdDetails[rdDetails.GetOrdinal("SHIPSEPARATE")]);
                                    ie.UPCCode = rdDetails[rdDetails.GetOrdinal("UPC")].ToString();
                                    ie.thirdPartySKU = rdDetails[rdDetails.GetOrdinal("TPSKU")].ToString();
                                    ie.harmonizationCode = Convert.ToInt32(rdDetails[rdDetails.GetOrdinal("HARMONIZATIONCODE")]);
                                    ie.createDate = Convert.ToDateTime(rdDetails[rdDetails.GetOrdinal("DATEX")]);
                                    ie.lastUpdatedBy = rdDetails[rdDetails.GetOrdinal("USERID")].ToString();
                                    ie.updateDate = Convert.ToDateTime(rdDetails[rdDetails.GetOrdinal("DATEX")]);
                                    // add this to the response object.
                                    response.Add(ie);
                                }
                            }
                            rdDetails.Close();
                        }
                        catch (InvalidCastException)
                        {
                            log.Error("Date conversion failed.");
                            cn.Close();
                            throw;
                        }
                        catch (SqlException ex)
                        {
                            log.Error("Ran into an issue with the " + account + " database.");
                            log.Error(cmd.CommandText.ToString());
                            log.Error(ex);
                            cn.Close();
                            throw;
                        }
                    }
                }
                // return the response object back the method caller
                return response;
            }
        }

        public DateTime GetStartDateTime()
        {
            // This retuns the StartDateTime control switch value.
            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    DateTime result = default(DateTime);

                    try
                    {
                        //generate the sql script to find last check time in controls datatable
                        cmd.CommandText = "SELECT AttributeValue FROM MISC..Controls WHERE ProgramName = 'BibiItemExtract' AND AttributeName = 'StartDateTime'";

                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();

                        //read what was returned
                        rd.Read();

                        //return what was found
                        result = Convert.ToDateTime(rd[0]);
                    }
                    catch (InvalidCastException)
                    {
                        log.Error("Date conversion failed.");
                        throw;
                    }
                    catch (SqlException ex)
                    {
                        log.Error("Could not get StartDate.");
                        log.Error(cmd.CommandText.ToString());
                        log.Error(ex);
                        throw;
                    }
                    finally
                    {
                        //close connection
                        cn.Close();
                    }
                    return result;
                }
            }
        }
        public DateTime GetEndDateTime()
        {
            // This retuns the EndDateTime control switch value.  If the control in not set, it will return the default "null" time.
            using (SqlConnection cn = new SqlConnection())
            {
                DateTime result = default(DateTime);

                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        //generate the sql script to find last check time in controls datatable
                        cmd.CommandText = "SELECT AttributeValue FROM MISC..Controls WHERE ProgramName = 'BibiItemExtract' AND AttributeName = 'EndDateTime'";

                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();

                        //read what was returned
                        rd.Read();

                        //return what was found
                        string Check = rd[0].ToString();

                        if (string.IsNullOrEmpty(Check) == false)
                        {
                            result = Convert.ToDateTime(rd[0]);
                        }
                    }
                    catch (InvalidCastException)
                    {
                        Console.WriteLine("Date conversion failed.");
                        throw;
                    }
                    catch (SqlException ex)
                    {
                        log.Error("Could not get EndDate.");
                        log.Error(cmd.CommandText.ToString());
                        log.Error(ex);
                        throw;
                    }
                    finally
                    {
                        //close connection
                        cn.Close();
                    }

                    return result;
                }
            }
        }

        public void UpdateDateTime(DateTime date)
        {
            // this method updates the date control switches for the next run.  It sets the new StartDateTime to the passed value and blanks out the EndDateTime
            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();

                        cmd.Connection = cn;
                        cmd.CommandText = "Update MISC..Controls SET AttributeValue = '" + date.ToString() + "' WHERE ProgramName = 'BibiItemExtract' AND AttributeName = 'StartDateTime'";
                        cmd.ExecuteNonQuery();

                        cmd.CommandText = "Update MISC..Controls SET AttributeValue = '' WHERE ProgramName = 'BibiItemExtract' AND AttributeName = 'EndDateTime'";
                        cmd.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        log.Error("Could not update control switches. " + Environment.NewLine + ex);
                    }
                    finally
                    {
                        //close connection
                        cn.Close();
                    }
                }
            }
        }
    }
}
