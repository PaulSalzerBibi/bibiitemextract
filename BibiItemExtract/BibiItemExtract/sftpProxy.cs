﻿using System;
using PWS.Mantis.DomainObjects.DataContract;
using PWS.Mantis.ServiceContractsAndProxies;
using log4net;


namespace BibiItemExtract
{
    public class sftpProxy
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private DistributionClient m_ClientProxy;
        private string m_SFtpHost;
        private int m_SFtpPort;
        private string m_SFtpUserName;
        private string m_SFtpPassword;
        private string m_StagingPath;


        public sftpProxy()
        {
            m_ClientProxy = new DistributionClient();
            m_SFtpHost = "ftp.ftpbythill.com";
            m_SFtpPort = 22;
            m_SFtpUserName = "collates%ftpbythill.com";
            m_SFtpPassword = "RgetVMDzesdy";
            m_StagingPath = Properties.Settings.Default.ftpStagePath.Trim().TrimEnd('/').TrimEnd('\\');
        }

        public void UploadFile(string fileName)
        {
            log.Debug("===========================================================");
            log.Debug("SFTPing the file");

            FileTransferDistributionRequest request = BuildFileUploadRequest(fileName);

            DistributionResponse response = m_ClientProxy.SubmitRequest(request);
            FileTransferDistributionResponse fileResponse = response as FileTransferDistributionResponse;
            if (fileResponse == null)
            {
                throw new Exception("File Response was null");
            }

            if (fileResponse.HasError)
            {
                string errorMessage = string.Empty;
                foreach (var message in fileResponse.ErrorMessages)
                {
                    errorMessage += message;
                }

                if (fileResponse.Exception != null)
                {
                    errorMessage += fileResponse.Exception.Message;
                }

                log.Error(errorMessage);
                throw new Exception(errorMessage);
            }

            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
            fi.Delete();
        }

        private FileTransferDistributionRequest BuildFileUploadRequest(string fileName)
        {
            return new FileTransferDistributionRequest()
            {
                Host = m_SFtpHost,
                Port = m_SFtpPort,
                UserName = m_SFtpUserName,
                Password = m_SFtpPassword,
                FileAction = FileTransferAction.Upload,
                ProtocolType = FileTransferProtocol.SFTP,
                Resource = fileName,
                RemoteDirectory = "EcomData",
            };
        }

        public string SFTPStagingPath()
        {
            return m_StagingPath;
        }
    }
}
