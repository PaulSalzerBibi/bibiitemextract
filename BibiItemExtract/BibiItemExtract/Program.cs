﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

[assembly: log4net.Config.XmlConfigurator(Watch=true)]

namespace BibiItemExtract
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        // Grabs the current DateTime which will be used as the default endDate.  It will also become the new startDate for the database control record. 
        static DateTime EDate = DateTime.Now;
        
        // Sets the file name and output directory
        static string outputFile = @"\ItemExtract_" + EDate.ToString("yyyyMMdd_HHmmssff") + ".csv";
        static string exportFile = Properties.Settings.Default.outputDirectory + outputFile;
        static string exportArchive = Properties.Settings.Default.outputDirectory + @"\Archive";
        
        static int Main()
        {

            log.Info("===========================================================");
            log.Info("Application started");

            System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
            log.Debug("===========================================================");
            log.Debug("Assembly identity = " + a.FullName);
            log.Debug("Codebase = " + a.CodeBase + Environment.NewLine);
            
            dbAccess db = new dbAccess();

            log.Debug("===========================================================");
            log.Debug("Getting control switch settings:");

            // Calls the method to grab the date from the control switch so that we will have the beginning date to pull shipments from
            DateTime startDate = db.GetStartDateTime();

            // set the ending date to the standard default date and then calls the method to check for a date in the control switch
            DateTime endDate = new DateTime();
            endDate = db.GetEndDateTime();

            // If no date is in the control switch, we will set the value to the default endDate
            if (endDate == DateTime.MinValue)
            {
                endDate = EDate;
            }

            log.Debug("");
            log.Info("startDate=" + startDate);
            log.Info("endDate=" + endDate + Environment.NewLine);
 
            log.Debug("===========================================================");
            log.Debug("Getting Active accounts");
            // grab active Ecometry accounts from MISC table.
            List<String> Accounts = db.getActiveAccounts();    
            
            log.Debug("===========================================================");
            log.Debug("Getting the Details of the inventory (transactions)");
            //grab the inventory transaction records between the two dates being passed.
            List<ItemExtract> ItemExtracts = db.getItemExtracts(Accounts, startDate, endDate);
            log.Info("Records found = " + ItemExtracts.Count);

            // initalize our line string
            String line = string.Empty;

            log.Debug("===========================================================");
            log.Debug("Creating output file");

            // create the output file
            using (StreamWriter sw = new StreamWriter(exportFile)) 
            {
                // set all the variables appropriately for each row found by the db.GetShipments method
                foreach (ItemExtract row in ItemExtracts)
                {
                    line = row.clientCode.TrimEnd() + ","
                     + row.thirdPartyID + ","
                     + row.productNumber.TrimEnd() + ","
                     + row.shortDescription.TrimEnd().Replace(",", "&#x2c;") + ","
                     + row.longDescription.TrimEnd().Replace(",", "&#x2c;") + ","
                     + row.productAlias + ","
                     + row.subWithProduct + ","
                     + row.category1.TrimEnd() + ","
                     + row.category2.TrimEnd() + ","
                     + row.category3.TrimEnd() + ","
                     + row.category4.TrimEnd() + ","
                     + row.available + ","
                     + row.notAvailableDate.ToShortDateString() + ","
                     + row.price + ","
                     + row.postageHandling + ","
                     + row.extraPostage + ","
                     + row.listPrice + ","
                     + row.productCost + ","
                     + row.quantityInStock + ","
                     + row.allocatedQuantity + ","
                     + row.itemStatus.TrimEnd() + ","
                     + row.lowStockLimit + ","
                     + row.reorderAmount + ","
                     + row.minOrderAmount + ","
                     + row.weight + ","
                     + row.height + ","
                     + row.width + ","
                     + row.length + ","
                     + row.taxExempt + ","
                     + row.shipSeparate + ","
                     + row.UPCCode.TrimEnd() + ","
                     + row.thirdPartySKU.TrimEnd() + ","
                     + row.harmonizationCode + ","
                     + row.backorderQuantity + ","
                     + row.createDate.ToShortDateString() + ","
                     + row.lastUpdatedBy.TrimEnd() + ","
                     + row.updateDate.ToShortDateString() + ","
                     + row.primaryLocation;
                    // write out the line
                    sw.WriteLine(line);
                }
             }
            
            sftpProxy ftp = new sftpProxy();

            System.IO.FileInfo fi = new System.IO.FileInfo(exportFile);
            string ftpStagedFile = ftp.SFTPStagingPath() + "\\" + fi.Name;

            try
            {
                System.IO.File.Copy(exportFile, ftpStagedFile);
            }
            catch (IOException ex)
            {
                log.Error("Error trying to copy file to DMZ. " + Environment.NewLine + ex);
            }
            
            ftp.UploadFile(ftpStagedFile);
            log.Debug ("");
            log.Info("Export file " + outputFile + " sent via SFTP");
            log.Debug("Output Directory = " + Properties.Settings.Default.outputDirectory);

            try
            {
                System.IO.File.Move(exportFile, exportArchive + outputFile);
            }
            catch (IOException ex)
            {
                log.Error("Error trying to archive sent file. " + Environment.NewLine + ex);
            }

            string[] files = Directory.GetFiles(exportArchive);

            foreach (string file in files)
            {
                //keeps only a days worth of archived files
                FileInfo archive = new FileInfo(file);
                if (archive.CreationTime < DateTime.Now.AddDays(-1))
                    archive.Delete();
            }

            db.UpdateDateTime(endDate);
            log.Debug("===========================================================");
            log.Info("Controls updated");
            log.Info("===========================================================");

            return 0;
        }
    }
}
