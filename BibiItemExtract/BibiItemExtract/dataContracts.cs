﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BibiItemExtract
{
    /********************************************************************************************************************************************
     *  Data Contracts
     ********************************************************************************************************************************************/

    [DataContract]
    public class ItemExtract
    {
        [DataMember]
        public string clientCode;
        [DataMember]
        public int thirdPartyID;
        [DataMember]
        public string productNumber;
        [DataMember]
        public string shortDescription;
        [DataMember]
        public string longDescription;
        [DataMember]
        public string primaryLocation;
        [DataMember]
        public string productAlias;
        [DataMember]
        public string subWithProduct;
        [DataMember]
        public string category1;
        [DataMember]
        public string category2;
        [DataMember]
        public string category3;
        [DataMember]
        public string category4;
        [DataMember]
        public int available;
        [DataMember]
        public DateTime notAvailableDate;
        [DataMember]
        public double price;
        [DataMember]
        public double postageHandling;
        [DataMember]
        public double extraPostage;
        [DataMember]
        public double listPrice;
        [DataMember]
        public double productCost;
        [DataMember]
        public int quantityInStock;
        [DataMember]
        public int allocatedQuantity;
        [DataMember]
        public string itemStatus;
        [DataMember]
        public int lowStockLimit;
        [DataMember]
        public int reorderAmount;
        [DataMember]
        public int minOrderAmount;
        [DataMember]
        public double weight;
        [DataMember]
        public double height;
        [DataMember]
        public double width;
        [DataMember]
        public double length;
        [DataMember]
        public int taxExempt;
        [DataMember]
        public int shipSeparate;
        [DataMember]
        public string UPCCode;
        [DataMember]
        public string thirdPartySKU;
        [DataMember]
        public int harmonizationCode;
        [DataMember]
        public int backorderQuantity;
        [DataMember]
        public DateTime createDate;
        [DataMember]
        public string lastUpdatedBy;
        [DataMember]
        public DateTime updateDate;
    }
}
